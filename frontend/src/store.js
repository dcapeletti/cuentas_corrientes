import Vue from 'vue'
import Vuex from 'vuex'

import {
  serviceMovimientosCuentaCorriente
} from '@/services/MovimientosCuentasCorrientes.js'
import {
  serviceCuentaCorriente
} from '@/services/CuentaCorriente.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cuentaCorrienteSeleccionada: {
      'id': '',
      'propietario': '',
      'nroCuenta': '',
      'tipoCuenta': '',
      'fechaCreacion': '',
      'saldo': ''
    },
    movimientos: [],
    filasTotales: 0,
    cuentas: [],
    error: ''
  },
  mutations: {
    seleccionarCuenta (state, registro) {
      state.cuentaCorrienteSeleccionada.id = registro.id
      state.cuentaCorrienteSeleccionada.propietario = registro.propietario
      state.cuentaCorrienteSeleccionada.nroCuenta = registro.nroCuenta
      state.cuentaCorrienteSeleccionada.tipoCuenta = registro.tipoCuenta
      state.cuentaCorrienteSeleccionada.fechaCreacion = registro.fechaCreacion
      // console.log(state.cuentaCorrienteSeleccionada)
      serviceMovimientosCuentaCorriente.contarMovimientos(state.cuentaCorrienteSeleccionada.id)
        .then((response) => {
          state.filasTotales = response.data.total
        })
        .catch((err) => {
          console.log(err)
        })

      serviceMovimientosCuentaCorriente.listarMovimientos(state.cuentaCorrienteSeleccionada.id, 1, 3)
        .then(response => {
          console.log('Movimientos', response)
          state.movimientos = response.data
        })
        .catch(e => {
          console.log(e)
        })
    },
    rellenarMovimientos (state, arrayMovimientos) {
      state.movimientos = arrayMovimientos
    },
    rellenarCuentas (state, cuentas) {
      state.cuentas = cuentas
    },
    agregarCuenta (state, cuenta) {
      state.cuentas.push(cuenta)
    },
    errorHandler (state, error) {
      state.error = error
    }
  },
  actions: {
    listarMovimientos ({commit}, currentPage, limite) {

    },
    guardarCuentaCorriente ({commit, dispatch}, {propietario, nroCuenta, moneda}) {
      serviceCuentaCorriente.crearCuentaCorriente(propietario, nroCuenta, moneda)
        .then(response => {
          console.log(response.data)
          commit('agregarCuenta', response.data)
        })
        .catch(error => {
          // console.log(error) //Aquí se puede llamar a un dispatch para actualizar un error o algo..
          alert('Mensaje de error: ' + error.response.data.error + '\n\nCausa: ' + error.response.data.message)
        })
    }
  }
})
