/* eslint-disable */
import axios from 'axios'

export const serverUrl = 'http://localhost:5000'

export const serviceMovimientosCuentaCorriente = {
  obtenerTiposMovimientos: function() {
    return axios.get(serverUrl + '/movimientos/tipo-movimiento')
  },
  crearMovimiento: function(cuenta, tipo_movimiento, importe, descripcion) {
    return axios.post(serverUrl + '/movimientos/agregar-movimiento/' + cuenta, {
      'importe': importe,
      'tipo_movimiento': tipo_movimiento,
      'descripcion': descripcion
    })
  },
  listarMovimientos: function(cuenta, pagina, limite){
    return axios.get(serverUrl + '/movimientos/listar-movimientos?cuenta='+ cuenta +'&page=' + pagina + '&limit=' + limite)
  },
  contarMovimientos: function (cuenta) {
    return axios.get(serverUrl + '/movimientos/contar/'+cuenta)
  }
}
