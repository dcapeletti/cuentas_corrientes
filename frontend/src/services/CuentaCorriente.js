/* eslint-disable */
import axios from 'axios'

export const serverUrl = 'http://localhost:5000'

export const serviceCuentaCorriente = {
  crearCuentaCorriente: function(propietarioCuentaCorriente, nroCuenta, tipoMoneda) {
    console.log("Creando cuenta corriente Nro: " + nroCuenta + " con moneda " + tipoMoneda)
    return axios.post(serverUrl + '/cuenta-corriente', {
      'propietario': propietarioCuentaCorriente,
      'nroCuenta': nroCuenta,
      'moneda': tipoMoneda
    })
  },

  eliminar: function(id) {
    console.log("Enviando peticion axios...")
    return axios.delete(serverUrl + '/cuenta-corriente/' + id)
  },

  obtenerCantidadRegistros: function() {
    //Retorna la cantidad de registros de la tabla...
    return axios.get(serverUrl + '/cuenta-corriente/count')
  },

  obtenerCuentasCorrientes: function(pagina, limite) {
    return axios.get(serverUrl + '/cuenta-corriente?page=' + pagina + '&limit=' + limite)
  },

  obtenerMonedas: function() {
    return axios.get(serverUrl + '/monedas')
  }
}
