import Vue from 'vue'
import Router from 'vue-router'

const routerOptions = [
  { path: '/', name: 'home', component: 'Home' },
  { path: '/about', name: 'about', component: 'About' },
  { path: '*', name: 'notfound', component: 'NotFound' }
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/views/${route.component}.vue`)
  }
})

Vue.use(Router)

export default new Router({
  routes,
  mode: 'history'
})
