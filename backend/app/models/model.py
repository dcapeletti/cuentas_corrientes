from app import db, ma
from datetime import datetime

class CuentaCorriente(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    propietario = db.Column(db.String(80), nullable=False)
    nroCuenta = db.Column(db.String(120), unique=True, nullable=False)
    tipo_moneda = db.Column(
        db.Integer, db.ForeignKey('moneda.id'), nullable=False)
    moneda = db.relationship('Moneda', backref='cuentas')
    saldo = db.Column(db.FLOAT(precision=10, scale=2), nullable=False)
    fechaCreacion = db.Column(db.DateTime, nullable=False)


class MovimientosCuentaCorriente(db.Model):
    # __tablename__ = 'MOVIMIENTOS_CUENTA_CORRIENTE'
    id = db.Column(db.Integer, primary_key=True)
    cuenta_id = db.Column(db.Integer, db.ForeignKey('cuenta_corriente.id'), nullable=False)
    cuenta = db.relationship('CuentaCorriente', backref='cuentas')
    tipo_movimiento = db.Column(db.Integer, db.ForeignKey(
        'tipo_movimiento.id'), nullable=False)
    movimiento = db.relationship('TipoMovimiento', backref='movimientos')
    descripcion = db.Column(db.String(200), nullable=False)
    importe = db.Column(db.FLOAT(precision=10, scale=2), nullable=False)
    fecha = db.Column(db.DateTime, nullable=False)


class TipoMovimiento(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80), nullable=False)


class Moneda(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80), nullable=False)


class CuentaCorrienteSchema(ma.ModelSchema):
    class Meta:
        model = CuentaCorriente


class MovimientosCuentaCorrienteSchema(ma.ModelSchema):
    class Meta:
        model = MovimientosCuentaCorriente


class TipoMovimientoSchema(ma.ModelSchema):
    class Meta:
        model = TipoMovimiento


class MonedaSchema(ma.ModelSchema):
    class Meta:
        model = Moneda


schema_cuenta_corriente = CuentaCorrienteSchema()
# Para paginacion de muchas cuentas
schema_cuentas_corriente = CuentaCorrienteSchema(many=True)

schema_movimiento_cuenta_corriente = MovimientosCuentaCorrienteSchema()
# Para paginacion de regultado de varios movimientos...
schema_movimientos_cuentas_corriente = MovimientosCuentaCorrienteSchema(
    many=True)

schema_tipo_movimiento = TipoMovimientoSchema()
# Para paginacion de varios tipos de movimientos...
schema_tipos_movimientos = TipoMovimientoSchema(many=True)

schema_moneda = MonedaSchema()
# Para paginacion de varios tipos de monedas...
schema_monedas = MonedaSchema(many=True)
