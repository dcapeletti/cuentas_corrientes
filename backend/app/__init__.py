from flask import Flask
from flask_cors import CORS
import requests
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

app = Flask(__name__,
            static_folder="../dist/static",
            template_folder="../dist")

app.config.from_object("config.DevelopmentConfig")

cors = CORS(app, resources={r"/api/*": {"origins": "*"},
                            r"/cuenta-corriente/*": {"origins": "*"},
                            r"/movimientos/*": {"origins": "*"},
                            r"/monedas/*": {"origins": "*"}})
# basedir = os.path.abspath(os.path.dirname(__file__))
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'crud.sqlite')

# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://cuentacorriente:cuentacorriente@localhost/cuentacorriente'

# print(app.config['SQLALCHEMY_DATABASE_URI'])
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# app.config['SQLALCHEMY_ECHO'] = False #Habilita la salida sql de sqlalchemy
db = SQLAlchemy(app)
ma = Marshmallow(app)

from app.routes import *
