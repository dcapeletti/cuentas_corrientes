from app import app, db
from flask import render_template, jsonify, request, make_response
from app.models.model import *
from sqlalchemy.exc import IntegrityError

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    if app.debug:
        return request.get('http://localhost:8080/{}'.format(path)).text
    return render_template("index.html")




# endpoint para crear nueva cuenta corriente...
'''
curl -X POST -H 'Content-Type: application/json' -i http://localhost:5000/cuenta-corriente --data '{
"propietario": "gfgd",
"nroCuenta": "654635",
"moneda": "2"
}'
'''
@app.route("/cuenta-corriente", methods=["POST"])
def add_cuenta_corriente():
    try:
        propietario = request.json['propietario']
        nroCuenta = request.json['nroCuenta']
        moneda = request.json['moneda']

        if propietario == "" or nroCuenta == "" or moneda == "":
            resp = make_response(jsonify(
                message='Datos incompletos o incorrectos.', error="Error al crear cuenta bancaria"), 400)
            return resp

        moneda = db.session.query(Moneda).get(int(moneda))
        nueva_cuenta_corriente = CuentaCorriente()
        nueva_cuenta_corriente.propietario = propietario
        nueva_cuenta_corriente.nroCuenta = nroCuenta
        nueva_cuenta_corriente.moneda = moneda
        nueva_cuenta_corriente.saldo = 0
        nueva_cuenta_corriente.fechaCreacion = datetime.utcnow()

        db.session.add(nueva_cuenta_corriente)
        db.session.commit()
        print("\nCuenta corriente " + nroCuenta + " creada correctamente...")
        return schema_cuenta_corriente.jsonify(nueva_cuenta_corriente)
    except KeyError as err:
        print("\nLas claves de la solicitud son incorrectas.")
        resp = make_response(jsonify(
            message='Las claves del objeto son incorrectas.', error="Error al crear cuenta bancaria"), 400)
        return resp
    except IntegrityError as err:
        print("\nHubo un error al crear la cuenta bancaria. Cuenta duplicada.")
        resp = make_response(jsonify(
            message='Cuenta duplicada', error="Error al crear cuenta bancaria"), 400)
        return resp
    except Exception as inst:
        print("Error de tipo:", type(inst))


# endpoint para obtener la cantidad de registros...
# curl -X GET -i http://localhost:5000/cuenta-corriente/count
@app.route("/cuenta-corriente/count", methods=["GET"])
def getCantidadRegistros():
    cantidad = db.session.query(CuentaCorriente).count()
    print("\nCantidad de registros: ", cantidad)
    return jsonify(total=cantidad)


# endpoint para mostrar las cuentas corrientes
# http://localhost:5000/cuenta-corriente?page=4&limit=5
# curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:5000/cuenta-corriente?page=1&limit=3'
@app.route("/cuenta-corriente", methods=["GET"])
def get_cuenta_corriente():
    page = request.args.get('page')
    limit = request.args.get('limit')
    print("Pagina", page, "limite", limit)
    cuentas_corriente = CuentaCorriente.query.paginate(
        int(page), int(limit), True)  # Objeto flask_sqlalchemy.Pagination
    result = schema_cuentas_corriente.dump(cuentas_corriente.items, many=True)
    return jsonify(result.data)


# endpoint para eliminar cuenta corriente...
# curl -X DELETE -i http://localhost:5000/cuenta-corriente/2
@app.route("/cuenta-corriente/<id>", methods=["DELETE"])
def eliminar_cuenta_corriente(id):
    # La cuenta solo se puede eliminar si NO tiene movimientos asociados...
    try:
        movimientos = db.session.query(
            MovimientosCuentaCorriente).filter_by(cuenta_id=int(id)).count()
        if movimientos > 0:
            resp = make_response(jsonify(message='La cuenta no puede eliminarse. Tiene ' + str(
                movimientos) + ' movimientos asociados.', error="Error al eliminar la cuenta bancaria"), 400)
            print("\nLa cuenta corriente no puede ser eliminada. Tiene ",
                  movimientos, " movimientos\n")
            return resp
        else:
            cuenta = CuentaCorriente.query.get(id)
            if cuenta != None:
                db.session.delete(cuenta)
                db.session.commit()
                print("\nCuenta corriente id: " + id + " eliminada.")
                return make_response(jsonify(title="Operacion correcta", message="Cuenta eliminada correctamente."), 200)
        return "No es posible eliminar la cuenta corriente " + id + ". Causa: No existe.\n"
    except Exception as err:
        print(err)
        return err


'''
curl -X POST -H 'Content-Type: application/json' -i http://localhost:5000/agregar-movimiento/9 --data '{
"tipo_movimiento": "1",
"descripcion": "prueba",
"importe": 45
}'
'''


@app.route("/movimientos/agregar-movimiento/<cuenta>", methods=["POST"])
def add_movimiento_cuenta_corriente(cuenta):
    try:
        importe = float(request.json['importe'])
        tipo_movimiento_post = request.json['tipo_movimiento']
        descripcion = request.json['descripcion']

        print("\n", importe, tipo_movimiento_post, descripcion)
        cuenta_corriente = CuentaCorriente.query.get(cuenta)
        tipo_movimiento = TipoMovimiento.query.get(tipo_movimiento_post)

        if cuenta_corriente != None:
            tipo_moneda_cuenta = db.session.query(CuentaCorriente).join(Moneda).filter(CuentaCorriente.id == cuenta)
        else:
            resp = make_response(jsonify(message='La cuenta corriente no existe.',
                                         error="Error al crear el movimiento"), 400)
            return resp

        tipo_moneda_cuenta = tipo_moneda_cuenta[0].moneda.nombre
        if tipo_moneda_cuenta == 'Peso' and importe > 1000:  # Para cualquier tipo de movimiento
            resp = make_response(jsonify(message='Las cuentas en pesos no pueden generar un movimiento mayor a 1000 unidades.',
                                         error="Error al crear el movimiento"), 400)
            return resp
        if tipo_moneda_cuenta == 'Dolar' and importe > 300: # Para cualquier tipo de movimiento
            resp = make_response(jsonify(message='Las cuentas en Dolares no pueden generar un movimiento mayor a 300 unidades.',
                                         error="Error al crear el movimiento"), 400)
            return resp
        if tipo_moneda_cuenta == 'Euro' and importe > 150: # Para cualquier tipo de movimiento
            resp = make_response(jsonify(message='Las cuentas en Euros no pueden generar un movimiento mayor a 150 unidades.',
                                         error="Error al crear el movimiento"), 400)
            return resp

        movimiento = MovimientosCuentaCorriente()
        movimiento.cuenta = cuenta_corriente
        movimiento.movimiento = tipo_movimiento
        movimiento.descripcion = descripcion
        movimiento.importe = importe
        movimiento.fecha = datetime.utcnow()
        db.session.add(movimiento)
        db.session.commit()

        print("\nSe ha agregado el movimiento a la cuenta", cuenta)
        return make_response(jsonify(message='El movimiento se ha creado correctamente.',
                                     title="Movimiento creado correctamente"), 200)
    except KeyError as err:
        print("\nLas claves de la solicitud son incorrectas.")
        resp = make_response(jsonify(
                message='Las claves del objeto son incorrectas.', error="Error al crear el movimiento."), 400)
        return resp
    except IndexError as err:
        resp = make_response(jsonify(
                message='Se ha encontrado un error. Es posible que la cuenta no exista',
                error="Error al crear el movimiento."), 400)
        return resp
    except Exception as err:
        resp = make_response(jsonify(
                message=err, error="Error al crear el movimiento."), 400)
        return resp

#Listar los movimientos de la cuenta X ordenados por fecha...
#curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:5000/movimientos/listar-movimientos?cuenta=1&page=1&limit=5'
@app.route("/movimientos/listar-movimientos", methods=["GET"])
def listar_movimiento_cuenta_corriente():
    try:
        cuenta = request.args.get('cuenta')
        page = request.args.get('page')
        limit = request.args.get('limit')
        print("\nListando los movimientos cuenta ", cuenta, "...Pagina", page, "limite", limit)
        movimientos = MovimientosCuentaCorriente.query.filter(
        MovimientosCuentaCorriente.cuenta_id==cuenta).order_by(
        MovimientosCuentaCorriente.fecha.desc()).paginate(
            int(page), int(limit), True)  # Objeto flask_sqlalchemy.Pagination
        result = schema_movimientos_cuentas_corriente.dump(movimientos.items, many=True)
        # return "dsd\n"
        return jsonify(result.data)
    except TypeError:
        return "Parametros incorrectos.\n"


#endpoint para contar los movimientos de una cuenta
#curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:5000/movimientos/contar/<cuenta>'
@app.route("/movimientos/contar/<cuenta>", methods=["GET"])
def getCantidadMovimientosCuenta(cuenta):
    cantidad = db.session.query(MovimientosCuentaCorriente).filter(MovimientosCuentaCorriente.cuenta_id==cuenta).count()
    print("\nCantidad de registros:", cantidad)
    return jsonify(total=cantidad)


#Endpoint para obtener las monedas
#curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:5000/monedas'
@app.route("/monedas", methods=["GET"])
def getMonedas():
    #Lista de todas las monedas para cargar en el form...
    monedas = Moneda.query.paginate(1, len(Moneda.query.all()), True)
    # Devuelve un array de objetos...
    result = schema_monedas.dump(monedas.items, many=True)
    print(result.data)
    return jsonify(result.data)

#endpoint para consultar movimientos de cuentas
#curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:5000/movimientos/tipo-movimiento'
@app.route("/movimientos/tipo-movimiento", methods=["GET"])
def listar_tipo_movimiento():
    #Lista de todas los movimientos para cargar en la lista
    tipos_movimiento = TipoMovimiento.query.paginate(1, len(TipoMovimiento.query.all()), True)
    result = schema_tipo_movimiento.dump(tipos_movimiento.items, many=True)
    print("\n\n", result.data, "\n\n") # Devuelve un array de objetos...
    return jsonify(result.data)
