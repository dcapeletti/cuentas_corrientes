from run import db, CuentaCorriente, MovimientosCuentaCorriente, CuentaCorrienteSchema, MovimientosCuentaCorrienteSchema, MonedaSchema, TipoMovimiento, TipoMovimientoSchema, Moneda

db.create_all()

moneda_schema = MonedaSchema()
moneda = Moneda(nombre="Dolar")
moneda2 = Moneda(nombre="Euro")

db.session.add(moneda)
db.session.add(moneda2)
db.session.commit()

cuenta_schema = CuentaCorrienteSchema()
movimientos_schema = MovimientosCuentaCorrienteSchema()

cuenta = CuentaCorriente(propietario="Diego Capeletti", nroCuenta="434242", moneda="2", fechaCreacion="2018-05-02")
movimiento = MovimientosCuentaCorriente(tipo_movimiento="2", fecha="2018-05-03", cuenta=cuenta, descripcion="Prueba", importe=23.56)
movimiento2 = MovimientosCuentaCorriente(tipo_movimiento="2", fecha="2018-02-28", cuenta=cuenta, descripcion="Otra prueba", importe=2343.76
)
movimiento3 = MovimientosCuentaCorriente(tipo_movimiento="2", fecha="2018-02-11", cuenta=cuenta, descripcion="Holaaa", importe=2343.76
)

db.session.add(cuenta)
db.session.add(movimiento)
db.session.add(movimiento2)
db.session.add(movimiento3)
db.session.commit()

//db.session.rollback() //para descartar los cambios

cuenta_schema.dump(movimiento).data
