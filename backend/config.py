
class Config(object):
    #Configuraciones x defecto
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://cuentacorriente:cuentacorriente@localhost/cuentacorriente'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False


class Production(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://cuentacorriente:cuentacorriente@localhost/cuentacorriente'
    DEBUG = False
    SECRET_KEY = "fdhfs"


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://cuentacorriente:cuentacorriente@localhost/cuentacorriente'
    SQLALCHEMY_ECHO = True
