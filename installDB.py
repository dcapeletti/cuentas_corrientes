from run import db, CuentaCorriente, MovimientosCuentaCorriente, CuentaCorrienteSchema, MovimientosCuentaCorrienteSchema, MonedaSchema, TipoMovimiento, TipoMovimientoSchema, Moneda

db.create_all()

monedaPeso = Moneda(nombre='Peso')
monedaDolar = Moneda(nombre='Dolar')
MonedaEuro = Moneda(nombre='Euro')

db.session.add(monedaPeso)
db.session.add(monedaDolar)
db.session.add(MonedaEuro)
db.session.commit()

credito = TipoMovimiento(nombre='Credito')
debito = TipoMovimiento(nombre='Debito')

db.session.add(credito)
db.session.add(debito)
db.session.commit()


trigger_actualizar_saldo = """
CREATE TRIGGER actualizarsaldo AFTER INSERT ON movimientos_cuenta_corriente
FOR EACH ROW
BEGIN
    DECLARE credito FLOAT default 0;
    DECLARE debito FLOAT default 0;

    set credito = (Select sum(importe) from movimientos_cuenta_corriente
        where movimientos_cuenta_corriente.tipo_movimiento=1 and movimientos_cuenta_corriente.cuenta_id=new.cuenta_id);

    set debito = (Select sum(importe) from movimientos_cuenta_corriente
        where movimientos_cuenta_corriente.tipo_movimiento=2 and movimientos_cuenta_corriente.cuenta_id=new.cuenta_id);

    if debito IS NULL then
        set debito=0;
    end if;
    if credito IS NULL then
        set credito =0;
    end if;

    UPDATE cuenta_corriente SET saldo = credito-debito WHERE cuenta_corriente.id = NEW.cuenta_id;
END
"""

prevenir_eliminacion_movimiento = """
CREATE TRIGGER prevenir_eliminacion BEFORE DELETE ON movimientos_cuenta_corriente
FOR EACH ROW
BEGIN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No está permitido eliminar movimientos.';
END
"""

prevenir_eliminacion_cuenta = """
CREATE TRIGGER prevenir_eliminacion_cuenta BEFORE DELETE ON cuenta_corriente
FOR EACH ROW
BEGIN
    DECLARE cantidad_reg INT;
    DECLARE msg VARCHAR(500);
    set cantidad_reg = (Select count(*) from cuenta_corriente inner join movimientos_cuenta_corriente on
        cuenta_corriente.id = movimientos_cuenta_corriente.cuenta_id where cuenta_corriente.id=old.id);
    if cantidad_reg > 0 THEN
        set msg = CONCAT('No está permitido eliminar una cuenta con ', cantidad_reg,' movimientos.'); 
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    END IF;
END
"""

db.engine.execute(trigger_actualizar_saldo)
db.engine.execute(prevenir_eliminacion_movimiento)
db.engine.execute(prevenir_eliminacion_cuenta)

print("Creación correcta de base de datos. \nPuede ejecutar flask ahora en un terminal con el siguiente comando:\nFLASK_APP=run.py FLASK_DEBUG=0 flask run")
