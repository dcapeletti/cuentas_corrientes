# Cuentas_Corrientes

Ejercicio resolución IT challenge cuentas corrientes y movimientos.

Frontend Vuejs

Backend Flask python

# REQUERIMIENTOS NECESARIOS

Tener python3 (3.5 o superior) instalado, pip3, virtualenv, nodejs 10.15 o superior,
vue CLI 3.3 o superior.


# GUIA DE INSTALACIÓN

Los siguientes comandos han sido ejecutados en GNU/Linux. No se han probado en Windows o Mac.

`pip3 install virtualenv`

`virtualenv -p python3 venv`

`source venv/bin/activate`

`git clone https://gitlab.com/dcapeletti/cuentas_corrientes`

`cd cuentas_corrientes`

`pip install -r requirements.txt`


# EJECUCIÓN DEL PROYECTO PARA DESARROLLO

Para ejecutar el proyecto modo desarrollo, debe estar en ejecución tanto el cliente como el servidor.
Primero debe arrancar o compilar el cliente para producción, luego ejecutar el servidor.

## Compilación del frontend para producción

Si no tiene instaladas las dependencias de npm, debe ubicarse en el directorio frontend y ejecutar el siguiente comando:

`cd frontend`

`npm install`

Luego, debe ubicarse en el directorio frontend y ejecutar:

`npm run build`


## Ejecución del frontend para desarrollo

Mientras está en modo de desarrollo, puede visualizar los cambios en el cliente sin necesidad de recargar el navegador. Para ello ubíquese en el directorio frontend y ejecute lo siguiente:

`npm install`

`npm run dev`



## Ejecución del servidor

En primer lugar, crear una base de datos en mysql llamada **cuentacorriente**, el usuario **cuentacorriente** con contraseña **cuentacorriente**.
Dentro del entorno virtual, ejecutar el archivo `python installDB.py` para crear todos los objetos en la BD.

Luego ubicarse en el directorio raíz cuenta_corriente. Debería ver un archivo llamado run.py. Debe ejecutar en ese path lo siguiente:

`FLASK_APP=run.py FLASK_DEBUG=1 flask run`

Si vas a usar a ejecutar el servidor en producción, poner FLASK_DEBUG=0
